<?php
	require_once 'db/outer/users.php';
	require_once 'db/outer/events.php';
	require_once 'db/outer/boards.php';
	require_once 'db/outer/messages.php';
	
    class eventRootQueryHandler{
	  
	    //non è detto che $requesterId sia settato
	    private $Users;
	    private $Events;
	    private $Boards;
	    private $Messages;
	
	    public function __construct(){
		    $this->Users = new Users($GLOBALS["dbh"]->db);
	        $this->Events = new Events($GLOBALS["dbh"]->db);
            $this->Boards = new Boards($GLOBALS["dbh"]->db);
   	        $this->Messages = new Messages($GLOBALS["dbh"]->db);	
        }
		

		private function boardQuery($eventId, $queryString, $requesterId){
			$regex = "/^(?P<brdQuery>(?P<brdName>[a-zA-Z]+)|(?P<brdId>\d+))\/((?P<brdMessages>messages\/)|$)/";
			$res = array();
			preg_match($regex, $queryString, $res, PREG_UNMATCHED_AS_NULL);
			switch($_SERVER['REQUEST_METHOD']){
				case "POST" :
					if(isSet($res["brdQuery"])){
						if(isSet($res["brdName"])){
							$boardId = $this->Boards->getId($eventId, $res["brdName"]);
							if(isSet($res["brdMessages"])){
								throw new Exception("eventRoot write message in board " . $boardId . " : " . $res["brdName"]);
							}else{
								throw new Exception("eventRoot meta update of board " . $boardId . " : " . $res["brdName"]);
							}
						}elseif(isSet($res["brdId"])){
							throw new Exception("eventRoot UNSUPPORTED , events/yyy/boards/IDIDID/...." . "userRootQueryHandler.boardQuery()");
						}
					}else{
						throw new Exception("eventRoot create new board for event " . $eventId);
					}
				break;
				case "GET" :
					if(isSet($res["brdQuery"])){
						if(isSet($res["brdName"])){
							$boardId = $this->Boards->getId($eventId, $res["brdName"]);
							if(isSet($res["brdMessages"])){
								echo $this->Messages->getAllByBoard($boardId);
							}else{
								echo $this->Boards->get($boardId);
							}
						}elseif(isSet($res["brdId"])){
							throw new Exception("eventRoot  UNSUPPORTED , events/yyy/boards/IDIDID/...." . "userRootQueryHandler.boardQuery()");
						}
					}else{
						echo $this->Boards->getByEvent($eventId);
					}
				break;			
				case "PUT" :
				break;
				case "DELETE" :
				break;
			}
		}

		private function eventSubQuery($eventId, $queryString, $requesterId){
			$regex = "/^(?P<evtSub>(?P<evtBoards>boards)|(?P<evtTickets>tickets)|(?P<evtAuthors>authors)|(?P<evtDates>dates)|(?P<evtPlaces>places))\/(?P<sub>\S+\/|$)/";
			$res = array();
			preg_match($regex, $queryString, $res, PREG_UNMATCHED_AS_NULL);
			switch($_SERVER['REQUEST_METHOD']){
				case "POST" :
					if(isSet($res["evtSub"])){
						if(isSet($res["evtBoards"])){
							if(isSet($res["sub"])){
								$this->boardQuery($eventId, $res["sub"], $requesterId);
							}else{
								throw new Exception("eventRoot POST create new board???");
								echo $this->Boards->getByEvent($eventId);
							}
						}elseif(isSet($res["evtTickets"])){
							throw new Exception("eventRoot POST eventTickets @" . "userRootQueryHandler.eventSubQuery()");
						}elseif(isSet($res["evtAuthors"])){
							throw new Exception("eventRoot POST eventAuthors @" . "userRootQueryHandler.eventSubQuery()");
						}elseif(isSet($res["evtPlaces"])){
							throw new Exception("eventRoot POST eventPlaces @" . "userRootQueryHandler.eventSubQuery()");
						}elseif(isSet($res["evtDates"])){
							throw new Exception("eventRoot POST eventDates @" . "userRootQueryHandler.eventSubQuery()");
						}else{
							throw new Exception("eventRoot POST shouldNotBeHere2 @" . "userRootQueryHandler.eventSubQuery()");
						}
					}else{
						throw new Exception("write something in events/".$eventId);
						echo $this->Events->get($eventId);
					}
				break;
				case "GET" :
					if(isSet($res["evtSub"])){
						if(isSet($res["evtBoards"])){
							if(isSet($res["sub"])){
								$this->boardQuery($eventId, $res["sub"], $requesterId);
							}else{
								echo $this->Boards->getByEvent($eventId);
							}
						}elseif(isSet($res["evtTickets"])){
							throw new Exception("eventRoot GET eventTickets @" . "userRootQueryHandler.eventSubQuery()");
						}elseif(isSet($res["evtAuthors"])){
							throw new Exception("eventRoot GET eventAuthors @" . "userRootQueryHandler.eventSubQuery()");
						}elseif(isSet($res["evtPlaces"])){
							throw new Exception("eventRoot GET eventPlaces @" . "userRootQueryHandler.eventSubQuery()");
						}elseif(isSet($res["evtDates"])){
							throw new Exception("eventRoot GET eventDates @" . "userRootQueryHandler.eventSubQuery()");
						}else{
							throw new Exception("eventRoot GET shouldNotBeHere2 @" . "userRootQueryHandler.eventSubQuery()");
						}
					}else{
						echo $this->Events->get($eventId);
					}
				break;
				case "PUT" :
				break;
				case "DELETE" :
				break;
			}
		}


		public function solve( $queryString, $requesterId){
			$regex = "/^(?P<evtQuery>(?P<evtName>[a-zA-Z]+)|(?P<evtId>\d+))\/(?P<evtSub>\S+|$)/";
			$res = array();
			preg_match($regex, $queryString, $res, PREG_UNMATCHED_AS_NULL);
			switch($_SERVER['REQUEST_METHOD']){
				
				case "POST" :
					if(isSet($res["evtQuery"])){
						if(isSet($res["evtName"])){
							throw new Exception("eventRoot post by name ". $res["evtName"] ."(unsupported)");
							if(isSet($res["evtSub"])){
								$this->eventSubQuery($eventId, $res["evtSub"], $requesterId);
							}else{
								throw new Exception("eventRoot write something in event name: ".$res["evtName"]);
								echo $this->Events->get($eventId); //non ci arriva mai, intercettato in eventSubQuery
							}
						}elseif(isSet($res["evtId"])){
							throw new Exception("eventRoot write something in event id :".$res["evtId"]);
						}else{
							throw new Exception("PHP SAYS : i should not be here @" . "userRootQueryHandler.eventQuery()");
						}
					}else{
						throw new Exception("PHP SAYS : POST what am i reading? @" . "userRootQueryHandler.eventQuery()");
					}
				break;
				
				case "GET" :
					if(isSet($res["evtQuery"])){
						if(isSet($res["evtName"])){
							throw new Exception("eventRoot GET all by Name " . $res["evtName"]);
						}elseif(isSet($res["evtId"])){
							$eventId = $res["evtId"];
							if(isSet($res["evtSub"])){
								$this->eventSubQuery($eventId, $res["evtSub"], $requesterId);
							}else{
								echo $this->Events->get($eventId); //non ci arriva mai, intercettato in eventSubQuery
							}
						}else{
							throw new Exception("eventRoot GET SHOULD NOT BE HERE");
						}
					}else{
						echo $this->Events->getAll();
					}
				break;
				
				case "PUT" :
				    throw new Exception("eventRoot PUT actions UNIMPLEMENTED");
				break;
				
				case "DELETE" :
				    throw new Exception("eventRoot DELETE actions UNIMPLEMENTED");
				break;
			}
			
		}
        		
	}
?>