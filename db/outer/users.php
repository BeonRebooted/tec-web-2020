<?php
    require_once "db/utils.php";
    class Users {
		private $db;
		
		public function __construct($database){
            $this->db = $database;
        }
		
		public function create($type, $email, $username, $password){
			$query = "INSERT INTO users (type, email, username, password) VALUES (?, ?, ?, ?)";
			$statement = $this->db->prepare($query);
		    $statement->bind_param("ssss", $type, $email, $username, $password);
            $errors = $statement->execute();
			if(!$errors){
				$id = $this->db->insert_id;
				return $this->get($id);
			}else{
				return json_failure("usr/create went bad");
			}
		}
		
		public function edit($who, $userData, $whoAsks){
			return json_failure();
		}
		
		public function close($who, $whoAsks){
			if($who == $whoAsks || $this->type($whoAsks) == "ADMIN"){
				return json_success("not closed for real");
			}
			return json_failure();
		}
		
		public function get($who){
			$statement = "SELECT id,type,username,email FROM users WHERE id = ?";
			$statement = $this->db->prepare($statement);
		    $statement->bind_param("i", $who);
            $statement->execute();
            $result = $statement->get_result();
            return json_encapsulate("user" , $result->fetch_all(MYSQLI_ASSOC));
		}
		
		public function getAll($whoAsks){
			$statement = "SELECT id,type,username,email FROM users";
			$statement = $this->db->prepare($statement);
            $statement->execute();
            $result = $statement->get_result();
			return json_encapsulate("userlist" , $result->fetch_all(MYSQLI_ASSOC));
		}

        public function getByName($nameQuery){
			$like = "%".$nameQuery."%";
			$statement = "SELECT id,type,username,email FROM users WHERE username LIKE ?";
			$statement = $this->db->prepare($statement);
			$statement->bind_param("s", $like);
            $statement->execute();
            $result = $statement->get_result();
			return json_encapsulate("userlist" , $result->fetch_all(MYSQLI_ASSOC));
		}

		function type($who){
			$statement = "SELECT type FROM users WHERE id = ?";
			$statement = $this->db->prepare($statement);
		    $statement->bind_param("i", $who);
            $statement->execute();
            $result = $statement->get_result();   
			return $result->fetch_assoc()["type"];
		}

        function authenticate($email, $password){
			$statement = "SELECT id FROM users WHERE email = ? AND password = ?";
			$statement = $this->db->prepare($statement);
		    $statement->bind_param("ss", $email, $password);
            $statement->execute();
            $result = $statement->get_result();   
			return $result->fetch_assoc()["id"];
		}
	}
?>