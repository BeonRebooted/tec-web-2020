<?php 
  require_once "db/utils.php";
  class messages {
	  
	  private $db;
  	  public function __construct($db){
            $this->db = $db;
      }
	  
	  function create($where, $what, $whoAsks){
		  $statement = "INSERT INTO messages (boardId, content, authorId) VALUES (?, ?, ?)";
          $statement = $this->db->prepare($statement);
		  $statement->bind_param("isi", $where, $what, $whoAsks);
		  $statement->execute();
		  return json_success($whoAsks . " wrote '" . $what . "' in " . $where);
	  }
	  
	  function get($which, $whoAsks){
		  $statement = "SELECT * FROM messages WHERE id = ?";
          $statement = $this->db->prepare($statement);
		  $statement->bind_param("i", $which);
		  $result = $statement->execute();
		  return json_encapsulate("message", $result->fetch_all(MYSQLI_ASSOC));
	  }
	  
	  function getAllByBoard($boardId){
		$lev3 ="SELECT * FROM messages ".
		       "WHERE boardId = ?";
		$statement = $this->db->prepare($lev3);
		$statement->bind_param("i", $boardId);
        $statement->execute();
        $result = $statement->get_result();
		return json_encapsulate("messagelist", $result->fetch_all(MYSQLI_ASSOC));		    
	  }
	  
      function edit($which, $what, $whoAsks){
		  $statement = "UPDATE messages SET content = ? WHERE (authorId = ? AND id = ?)";
          $statement = $this->db->prepare($statement);
		  $statement->bind_param("sii", $what, $whoAsks, $which);
		  $statement->execute();  
		  return json_success();
	  }
	  
	  function report($which, $reason, $whoAsks){
		  $statement = "INSERT INTO reports reporterId, targetId, targetType, reason VALUES ?, ?, ?, ?";
          $statement = $this->db->prepare($statement);
		  $statement->bind_param("iiss", $whoAsks, $which, "message", $reason);
		  $statement->execute();
		  return json_failure();
	  }
  }
?>