<?php
    require_once "db/utils.php";
	require_once 'db/outer/boards.php';
    require_once 'db/outer/users.php';
	
	class Events{
	  private $db;	
	  private $Boards;
	  private $Users;
		
      public function __construct( $database){
        $this->db = $database;
		$this->Boards = new Boards($this->db);
		$this->Users = new Users($this->db);
      }
    
      public function get( $id){
        $lev3 ="SELECT * FROM events ".
		       "JOIN eventauthors ".
			   "ON events.id = eventauthors.eventId ".
			   "JOIN eventlogos ".
			   "ON eventlogos.eventId = eventauthors.eventId ".
			   "JOIN eventplaceandtime ".
			   "ON eventlogos.eventId = eventplaceandtime.eventId ".
			   "WHERE id = ?";
        $statement = $this->db->prepare($lev3);
    	$statement->bind_param("i", $id);
        $statement->execute();
        $result = $statement->get_result();
		return json_encapsulate("event" , $result->fetch_all(MYSQLI_ASSOC));
      }  //usable by any
	  
	  public function getAll(){
		$statement = "SELECT * FROM events JOIN eventAuthors ON events.id = eventAuthors.eventId";
		$statement = $this->db->prepare($statement);
        $statement->execute();
        $result = $statement->get_result();
		return json_encapsulate("eventlist" , $result->fetch_all(MYSQLI_ASSOC));
		}
    
      public function getByName( $nameQuery){
	    $like = "%".$nameQuery."%";
		$lev3 ="SELECT * FROM events ".
		       "JOIN eventauthors ".
			   "ON events.id = eventauthors.eventId ".
			   "JOIN eventlogos ".
			   "ON eventlogos.eventId = eventauthors.eventId ".
			   "JOIN eventplaceandtime ".
			   "ON eventlogos.eventId = eventplaceandtime.eventId ".
			   "WHERE name LIKE ?";
		$statement = $this->db->prepare($lev3);
		$statement->bind_param("s", $like);
        $statement->execute();
        $result = $statement->get_result();
		return json_encapsulate("eventlist" , $result->fetch_all(MYSQLI_ASSOC));
		}
    
      public function getByAuthor( $id){
		$lev3 ="SELECT * FROM events ".
		       "JOIN eventauthors ".
			   "ON events.id = eventauthors.eventId ".
			   "JOIN eventlogos ".
			   "ON eventlogos.eventId = eventauthors.eventId ".
			   "JOIN eventplaceandtime ".
			   "ON eventlogos.eventId = eventplaceandtime.eventId ".
			   "WHERE authorId = ?";
		$statement = $this->db->prepare($lev3);
		$statement->bind_param("i", $id);
        $statement->execute();
        $result = $statement->get_result();
		return json_encapsulate("eventlist" , $result->fetch_all(MYSQLI_ASSOC));
	  }
    
      public function getByPlace( $where, $radiusUNUSED){
		$lev3 ="SELECT * FROM events ".
		       "JOIN eventauthors ".
			   "ON events.id = eventauthors.eventId ".
			   "JOIN eventlogos ".
			   "ON eventlogos.eventId = eventauthors.eventId ".
			   "JOIN eventplaceandtime ".
			   "ON eventlogos.eventId = eventplaceandtime.eventId ".
			   "WHERE place = ?";
		$statement = $this->db->prepare($lev3);
		$statement->bind_param("s",$where);
        $statement->execute();
        $result = $statement->get_result();
		return json_encapsulate("eventlist" , $result->fetch_all(MYSQLI_ASSOC));  
	  }
    
      public function getByDateRange( $start, $end){
		$lev3 ="SELECT * FROM events ".
		       "JOIN eventauthors ".
			   "ON events.id = eventauthors.eventId ".
			   "JOIN eventlogos ".
			   "ON eventlogos.eventId = eventauthors.eventId ".
			   "JOIN eventplaceandtime ".
			   "ON eventlogos.eventId = eventplaceandtime.eventId ".
			   "WHERE date BETWEEN ? AND ?";
		$statement = $this->db->prepare($lev3);
		$statement->bind_param("ss",$start, $end);
        $statement->execute();
        $result = $statement->get_result();
		return json_encapsulate("eventlist" , $result->fetch_all(MYSQLI_ASSOC));
	  }
	  
	  public function getByParams($params){
		  if(isSet($params["space"])){
			  //CITTA o punto sulla mappa
			  //RAGGIO
		  }
		  if(isSet($params["time"])){
			  //time range
		  }
		  if(isSet($params["keywords"])){
			  //filtra per tags
		  }
		  //ESEGUI UNA QUERY CON TUTTI I PARAMETRI TROVATI
	  }
	  
      public function create( $name, $description, $data, $place, $img, $whoAsks){
		$type = $this->Users->type($whoAsks);
	    if($type == "AUTHOR"){
		    $query = "INSERT INTO events (name, description) VALUES (?, ?)";
            $statement = $this->db->prepare($query);
		    $statement->bind_param("ss", $name, $description);
            $success = $statement->execute();
			$success = TRUE;
            if($success){
				$id = $this->db->insert_id;
                $query = "INSERT INTO eventAuthors (eventId, authorId) VALUES (?, ?)";  
                $statement = $this->db->prepare($query);
		        $statement->bind_param("ii", $id, $whoAsks);
                $success = $statement->execute();
				
				$query = "INSERT INTO eventPlaceAndTime (eventId, date, place) VALUES (?, ?, ?)";  
                $statement = $this->db->prepare($query);
		        $statement->bind_param("iss", $id, $data, $place);
                $success = $statement->execute();
				
                $query = "INSERT INTO eventLogos (eventId, imgSource, imgType) VALUES (?, ?, ?)";
                $statement = $this->db->prepare($query);
				$type = "front";
		        $statement->bind_param("iss", $id, $img, $type);
                $success = $statement->execute();
				
				$this->Boards->initEvent($id);
				return $this->get($id);
			}else{
				return json_failure();
			}
       	}else{
			return json_failure("not an Author");
		}
      }  //eventi multidata o multiluogo?
    
      public function closeEvent( $id, $whoAsks){}  //è un delete posticipato
    
      public function updateEvent( $id, $data, $whoAsks){}
    
      //l'autore è già disaccoppiato,garantire non ripetibilità su (authorId+evtName)   
      public function addAuthor( $id, $whoAsks){
		  //un autore non può avere due eventi con lo stesso nome
		  //un evento non può avere due autori con lo stesso id
	  }

      public function addBoard( $eventId, $boardName, $whoAsks){
		  if($this->isAuthor($whoAsks, $eventId)){
			  return $this->Boards->create($eventId, $boardName);
		  }else{
			  return json_failure();
		  }
		  
	  }

      public function getTicket( $eventId, $whoAsks){
		$lev3 = "INSERT INTO tickets (eventId, userId) VALUES ( ?, ?)";
		$statement = $this->db->prepare($lev3);
		$statement->bind_param("ii", $eventId, $whoAsks);
        $statement->execute();
        $result = $statement->get_result();
		//controllare realmente se si ha successo non è cosi semplice
		//devi prendere l'ultimo id creato (in questa transazione)
		//già che esista è un buon punto (è ticket.id)
		//verifichi che ESISTA la tripla id,evtId,userId
		return json_success();
	  }

      public function reportEvent( $id, $reason, $whoAsks){}  //ti hanno rubato il marchio?
    
      function isAuthor( $eventId, $whoAsks){
		$lev3 ="SELECT authorId FROM events ".
		       "JOIN eventauthors ".
			   "ON events.id = eventauthors.eventId ".
			   "WHERE eventId = ? AND authorId = ?";
		$statement = $this->db->prepare($lev3);
		$statement->bind_param("ii", $authorId, $whoAsks);
        $statement->execute();
        $result = $statement->get_result();
		return isSet($result->fetch_all(MYSQLI_ASSOC)[0]);		
	  }
        
      //è garantito da vincoli di dominio che authorId+evtName sia unico
      function getId( $authorId, $eventName){
		$lev3 ="SELECT id FROM events ".
		       "JOIN eventauthors ".
			   "ON events.id = eventauthors.eventId ".
			   "WHERE authorId = ? AND name = ?";
		$statement = $this->db->prepare($lev3);
		$statement->bind_param("is", $authorId, $eventName);
        $statement->execute();
        $result = $statement->get_result();
		$res2 = $result->fetch_all(MYSQLI_ASSOC);
		if(isSet($res2[0])){
			return $res2[0]["id"];
		}else{
			throw new Exception("Events->getId() FAILED");
		}
				  
	  }	  
	}
?>