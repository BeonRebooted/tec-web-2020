<?php
	require_once 'db/outer/users.php';
	require_once 'db/outer/events.php';
	require_once 'db/outer/boards.php';
	require_once 'db/outer/messages.php';
	
  class userRootQueryHandler{
	  
	//non è detto che $requesterId sia settato
	private $Users;
	private $Events;
	private $Boards;
	private $Messages;
	
	public function __construct(){
		$this->Users = new Users($GLOBALS["dbh"]->db);
	    $this->Events = new Events($GLOBALS["dbh"]->db);
        $this->Boards = new Boards($GLOBALS["dbh"]->db);
   	    $this->Messages = new Messages($GLOBALS["dbh"]->db);	
    }
	
	//per togliere lo slash finale dalle stringhe
	private function removeTail($string){
		return substr($string,0,-1);
	}

    private function boardQuery($eventId, $queryString, $requesterId){
		$regex = "/^(?P<brdQuery>(?P<brdName>[a-zA-Z]+)|(?P<brdId>\d+))\/((?P<brdMessages>messages\/)|$)/";
	    $res = array();
		preg_match($regex, $queryString, $res, PREG_UNMATCHED_AS_NULL);
		switch($_SERVER['REQUEST_METHOD']){
		    case "POST" :
				if(isSet($res["brdQuery"])){
					if(isSet($res["brdName"])){
						$boardId = $this->Boards->getId($eventId, $res["brdName"]);
						if(isSet($res["brdMessages"])){
							throw new Exception("write message in board " . $boardId . " : " . $res["brdName"]);
						}else{
							throw new Exception("meta update of board " . $boardId . " : " . $res["brdName"]);
						}
					}elseif(isSet($res["brdId"])){
						throw new Exception("PHP SAYS : UNSUPPORTED , usrs/xxx/events/yyy/boards/IDIDID/...." . "userRootQueryHandler.boardQuery()");
					}
				}else{
					throw new Exception("create new board for event " . $eventId);
				}
			break;
		    case "GET" :
			    if(isSet($res["brdQuery"])){
					if(isSet($res["brdName"])){
						$boardId = $this->Boards->getId($eventId, $res["brdName"]);
						if(isSet($res["brdMessages"])){
							echo $this->Messages->getAllByBoard($boardId);
						}else{
							echo $this->Boards->get($boardId);
						}
					}elseif(isSet($res["brdId"])){
						throw new Exception("PHP SAYS : UNSUPPORTED , usrs/xxx/events/yyy/boards/IDIDID/...." . "userRootQueryHandler.boardQuery()");
					}
				}else{
					echo $this->Boards->getByEvent($eventId);
				}
            break;			
		    case "PUT" :
            break;
		    case "DELETE" :
            break;
		}
	}

    private function eventSubQuery($eventId, $queryString, $requesterId){
		$regex = "/^(?P<evtSub>(?P<evtBoards>boards)|(?P<evtTickets>tickets)|(?P<evtAuthors>authors)|(?P<evtDates>dates)|(?P<evtPlaces>places))\/(?P<sub>\S+\/|$)/";
	    $res = array();
	    preg_match($regex, $queryString, $res, PREG_UNMATCHED_AS_NULL);
		switch($_SERVER['REQUEST_METHOD']){
		    case "POST" :
			    if(isSet($res["evtSub"])){
					if(isSet($res["evtBoards"])){
						if(isSet($res["sub"])){
							$this->boardQuery($eventId, $res["sub"], $requesterId);
						}else{
							throw new Exception("create new board???");
							echo $this->Boards->getByEvent($eventId);
						}
					}elseif(isSet($res["evtTickets"])){
						throw new Exception("PHP SAYS : POST eventTickets @" . "userRootQueryHandler.eventSubQuery()");
					}elseif(isSet($res["evtAuthors"])){
						throw new Exception("PHP SAYS : POST eventAuthors @" . "userRootQueryHandler.eventSubQuery()");
					}elseif(isSet($res["evtPlaces"])){
						throw new Exception("PHP SAYS : POST eventPlaces @" . "userRootQueryHandler.eventSubQuery()");
					}elseif(isSet($res["evtDates"])){
						throw new Exception("PHP SAYS : POST eventDates @" . "userRootQueryHandler.eventSubQuery()");
					}else{
						throw new Exception("PHP SAYS : POST shouldNotBeHere2 @" . "userRootQueryHandler.eventSubQuery()");
					}
				}else{
					throw new Exception("write something in events/".$eventId);
					echo $this->Events->get($eventId);
				}
			break;
		    case "GET" :
			    if(isSet($res["evtSub"])){
					if(isSet($res["evtBoards"])){
						if(isSet($res["sub"])){
							$this->boardQuery($eventId, $res["sub"], $requesterId);
						}else{
							echo $this->Boards->getByEvent($eventId);
						}
					}elseif(isSet($res["evtTickets"])){
						throw new Exception("PHP SAYS : GET eventTickets @" . "userRootQueryHandler.eventSubQuery()");
					}elseif(isSet($res["evtAuthors"])){
						throw new Exception("PHP SAYS : GET eventAuthors @" . "userRootQueryHandler.eventSubQuery()");
					}elseif(isSet($res["evtPlaces"])){
						throw new Exception("PHP SAYS : GET eventPlaces @" . "userRootQueryHandler.eventSubQuery()");
					}elseif(isSet($res["evtDates"])){
						throw new Exception("PHP SAYS : GET eventDates @" . "userRootQueryHandler.eventSubQuery()");
					}else{
						throw new Exception("PHP SAYS : GET shouldNotBeHere2 @" . "userRootQueryHandler.eventSubQuery()");
					}
				}else{
					echo $this->Events->get($eventId);
				}
			break;
		    case "PUT" :
			break;
		    case "DELETE" :
			break;
		}
	}

    private function eventQuery( $userId, $queryString, $requesterId){
		$regex = "/^(?P<evtQuery>(?P<evtName>[a-zA-Z]+)|(?P<evtId>\d+))\/(?P<evtSub>\S+|$)/";
	    $res = array();
	    preg_match($regex, $queryString, $res, PREG_UNMATCHED_AS_NULL);
		switch($_SERVER['REQUEST_METHOD']){
			case "POST" :
			    if(isSet($res["evtQuery"])){
					if(isSet($res["evtName"])){
                        $eventId = $this->Events->getId( $userId, $res["evtName"]);
						if(isSet($res["evtSub"])){
							$this->eventSubQuery($eventId, $res["evtSub"], $requesterId);
						}else{
							throw new Exception("write something in event name: ".$res["evtName"]);
							echo $this->Events->get($eventId); //non ci arriva mai, intercettato in eventSubQuery
						}
					}elseif(isSet($res["evtId"])){
                        throw new Exception("write something in event id :".$res["evtId"]);
					}else{
						throw new Exception("PHP SAYS : i should not be here @" . "userRootQueryHandler.eventQuery()");
					}
				}else{
					throw new Exception("PHP SAYS : POST what am i reading? @" . "userRootQueryHandler.eventQuery()");
				}
			break;
			case "GET" :
			    if(isSet($res["evtQuery"])){
					if(isSet($res["evtName"])){
                        $eventId = $this->Events->getId( $userId, $res["evtName"]);
						if(isSet($res["evtSub"])){
							$this->eventSubQuery($eventId, $res["evtSub"], $requesterId);
						}else{
							echo $this->Events->get($eventId); //non ci arriva mai, intercettato in eventSubQuery
						}
					}elseif(isSet($res["evtId"])){
                        throw new Exception("PHP SAYS : users/xxx/events/ididid/ UNFINISHED @" . "userRootQueryHandler.eventQuery()");
					}else{
						throw new Exception("PHP SAYS : i should not be here @" . "userRootQueryHandler.eventQuery()");
					}
				}else{
					throw new Exception("PHP SAYS : GET what am i reading? @" . "userRootQueryHandler.eventQuery()");
				}
			break;
			case "PUT" :
			break;
			case "DELETE" :
			break;
		}
		
	}
	
	private function userSubQuery( $userId, $queryString, $requesterId){
		//gestisce (events/???)|(tickets/???)|(bookmarks/???)
		//non è detto che $requesterId sia settato
		$regex = "/^(?P<usrEvents>events\/($|(?P<evtQuery>\S+)))|(?P<usrTickets>tickets\/($|(?P<tckQuery>\S+)))|(?P<usrBookmarks>bookmarks\/($|(?P<bmkQuery>\S+)))/";
		$res = array();
	    preg_match($regex, $queryString, $res, PREG_UNMATCHED_AS_NULL);
		switch($_SERVER['REQUEST_METHOD']){
			case "POST" :
			    if(isSet($res["usrEvents"])){
			       if(isSet($res["evtQuery"])){
					   $this->eventQuery( $userId, $res["evtQuery"], $requesterId);
				   }else{
					   throw new Exception("create new event for user ". $userId ." @" . "userRootQueryHandler.userSubQuery()");
					   echo $this->Events->getByAuthor($userId);
				   }
			   }elseif(isSet($res["usrTickets"])){
			        throw new Exception("create new user ticket??? @" . "userRootQueryHandler.userSubQuery()");
		        }elseif(isSet($res["usrBookmarks"])){
			        throw new Exception("create new user bookmark @" . "userRootQueryHandler.userSubQuery()");
		        }else{
			        throw new Exception("PHP says : bad subURI @" . "userRootQueryHandler.userSubQuery()");
		        }
			break;
			case "GET" :
			    if(isSet($res["usrEvents"])){
			       if(isSet($res["evtQuery"])){
					   $this->eventQuery( $userId, $res["evtQuery"], $requesterId);
				   }else{
					   echo $this->Events->getByAuthor($userId);
				   }
		        }elseif(isSet($res["usrTickets"])){
                    throw new Exception("PHP SAYS : GET usr/ididid/tickets/ UNIMPLEMENTED @" . "userRootQueryHandler.userSubQuery()");
		        }elseif(isSet($res["usrBookmarks"])){
			        throw new Exception("PHP SAYS : GET usr/ididid/bookmarks/ UNIMPLEMENTED @" . "userRootQueryHandler.userSubQuery()");
		        }else{
			        throw new Exception("PHP says : bad subURI @" . "userRootQueryHandler.userSubQuery()");
		        }
			break;
			case "PUT" :
			    throw new Exception("PHP SAYS : UNIMPLEMENTED @" . "userRootQueryHandler.userSubQuery()");
			break;
			case "DELETE" :
			    throw new Exception("PHP SAYS : UNIMPLEMENTED @" . "userRootQueryHandler.userSubQuery()");
			break;
		}

	}
	
	//interpreta solo il primo segmento per capire se è un id,un nome,o è vuoto
	public function solve($queryString, $requesterId){
		//non è detto che $requesterId sia settato
		$regex = "/^(?P<legitRoot>$|((?P<usrName>[a-zA-Z]+)\/$)|((?P<usrId>\d+)\/)((?P<usrQuery>\S+)|$))/";
		$res = array();
	    preg_match($regex, $queryString, $res, PREG_UNMATCHED_AS_NULL);
		if(isSet($res["legitRoot"])){
			switch($_SERVER['REQUEST_METHOD']){
				case "POST" :
					if(isSet($res["usrName"])){
						throw new Exception("write something in user named " . $res["usrName"]);
					    echo $this->Users->getByName($res["usrName"]);
					}elseif(isSet($res["usrId"])){
						$userId = $res["usrId"];
						if(isSet($res["usrQuery"])){
						    $this->userSubQuery( $userId, $res["usrQuery"], $requesterId);
						}else{
							throw new Exception("write something in user id: " . $userId);
							echo $this->Users->get($userId);
						}
					}else{
						throw new Exception("create new user");
						echo $this->Users->getAll($requesterId);
					}
				break;
				case "GET" :
					if(isSet($res["usrName"])){
					    echo $this->Users->getByName($res["usrName"]);
					}elseif(isSet($res["usrId"])){
						$userId = $res["usrId"];
						if(isSet($res["usrQuery"])){
						    $this->userSubQuery( $userId, $res["usrQuery"], $requesterId);
						}else{
							echo $this->Users->get($userId);
						}
					}else{
						echo $this->Users->getAll($requesterId);
					}
				break;
				case "PUT" :
				    throw new Exception("PUT actions UNIMPLEMENTED");
				break;
				case "DELETE" :
				    throw new Exception("DELETE actions UNIMPLEMENTED");
				break;
			}
		}else{
			throw new Exception("PHP SAYS : bad URI userRootQueryHandler");
		}
	}
  }
 ?>