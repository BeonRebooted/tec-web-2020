<?php
  require_once "db/utils.php";
  class Boards{
	private $db;
	
	public function __construct($db){
        $this->db = $db;
    }
	
	public function initEvent($evtId){
        $this->create($evtId, "main");
		$this->create($evtId, "faq");
		$this->create($evtId, "after");
	}
	
	public function create($evtId, $name){
	    $query = "INSERT INTO boards (eventId, name) VALUES (?, ?)";
        $statement = $this->db->prepare($query);
        $statement->bind_param("is", $evtId, $name);
        $success = $statement->execute();
        return json_success("board created : " . $name);
	}
	
	public function getAll(){
	}
	
	public function getByEvent($eventId){
		$query = "SELECT * FROM boards WHERE eventId = ?";
        $statement = $this->db->prepare($query);
		$statement->bind_param("i", $eventId);
        $success = $statement->execute();
		$result = $statement->get_result();
		return json_encapsulate("boardlist", $result->fetch_all(MYSQLI_ASSOC));
		
	}
	
	public function get($id){
		$lev3 ="SELECT * FROM boards ".
			   "WHERE id = ?";
		$statement = $this->db->prepare($lev3);
		$statement->bind_param("i", $id);
        $statement->execute();
        $result = $statement->get_result();
		return json_encapsulate( "board", $result->fetch_all(MYSQLI_ASSOC));		
	}
	
	function getId($eventId, $boardName){
		$lev3 ="SELECT id FROM boards ".
			   "WHERE eventId = ? AND name = ?";
		$statement = $this->db->prepare($lev3);
		$statement->bind_param("is", $eventId, $boardName);
        $statement->execute();
        $result = $statement->get_result();
		return $result->fetch_all(MYSQLI_ASSOC)[0]["id"];		  
	}
  }
?>