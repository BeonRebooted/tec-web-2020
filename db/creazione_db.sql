SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- si usa InnoDB di default, non c'è bisogno della direttiva

-- -----------------------------------------------------
-- Schema EventsNOW
-- -----------------------------------------------------
DROP DATABASE IF EXISTS `EventsNOW`;
CREATE SCHEMA IF NOT EXISTS `EventsNOW` DEFAULT CHARACTER SET utf8 ;
USE `EventsNOW` ;

CREATE TABLE IF NOT EXISTS `EventsNOW`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(64) NOT NULL,			-- User Event Admin
  `username` VARCHAR(64) NOT NULL,
  `email` VARCHAR(64) NOT NULL,
  `password` VARCHAR(64) NOT NULL,
  PRIMARY KEY (`id`)
);
  
CREATE TABLE IF NOT EXISTS `EventsNOW`.`tickets`(
  `id` INT NOT NULL AUTO_INCREMENT,
  `eventId` INT NOT NULL,
  `userId` INT NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `EventsNOW`.`events` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(256),
  `description` VARCHAR(256),  
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `EventsNOW`.`eventAuthors` (
  `eventId` INT NOT NULL,
  `authorId` INT NOT NULL);
  
CREATE TABLE IF NOT EXISTS `EventsNOW`.`eventPlaceAndTime` (
  `eventId` INT NOT NULL,
  `date` TIMESTAMP NOT NULL,
  `place` VARCHAR(128) NOT NULL
);

CREATE TABLE IF NOT EXISTS `EventsNOW`.`eventLogos` (
  `eventId` INT NOT NULL,
  `imgSource` VARCHAR(64) NOT NULL,
  `imgType` VARCHAR(10) NOT NULL
);

CREATE TABLE IF NOT EXISTS `EventsNOW`.`boards` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `eventId` INT NOT NULL,
  `name` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `EventsNOW`.`messages` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `authorId` INT NOT NULL ,
  `boardId` INT NOT NULL ,
  `content` TEXT NOT NULL,
  `creation` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `hasImages` BOOLEAN NOT NULL,
  `closed` BOOLEAN NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `EventsNOW`.`messageImages` (
  `messageId` INT NOT NULL,
  `index` INT NOT NULL,
  `imgSource` VARCHAR(256) NOT NULL
);

CREATE TABLE IF NOT EXISTS `EventsNOW`.`reports` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `reporterId` INT NOT NULL ,
  `targetId` INT NOT NULL,
  `targetType` VARCHAR(30) NOT NULL,
  `reason` VARCHAR(256),
  PRIMARY KEY (`id`)
);

SET time_zone='+00:00';
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;