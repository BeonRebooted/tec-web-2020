<?php
    function isAdmin(){}
	
	function json_encapsulate($dataType, $data){
		$arr = array("type" => $dataType , "data" => $data);
        return json_encode($arr);
	}

    function json_success($args = "BOH"){
        return json_encapsulate("RESULT","SUCCESS");
    }
	
    function json_failure($args = "BOH"){
        return json_encapsulate("RESULT","ERROR");
    }
	
    function json_notFound($args = "BOH"){
        return json_encapsulate("RESULT","NOT FOUND");
    }
	

	
	function json_schema(){
		$schema = [
		    "user" => [
			    "id" => "1",
			    "type" => "USER/AUTHOR/ADMIN",
			    "username" => "anyName",
			    "email" => "your@email.com",
			    "password" => "SPOSTA ALTROVE",
			    "tickets" => ["ticket1", "ticket300"],    //SOLO USER
			    "users" => ["2","3","4","10","18"],    //segui solo gli AUTHOR
			    "events" => ["22","33","44","1010","1818"],    //TUTTI I type
			    "keywords" => ["YOLO","PRRRAH","CINEMASTENFI"]  //TUTTI I type
			],

		    "event" => [
			    "id" => "1",
			    "name" => "nameUNIQUE for AUTHOR",
			    "description" => "blablablablalabla",
			    "images" => [
				    "vertical" => ["src" => "img1.jpg" , "data" => "xxxxxxxxxxxxxx"],
				    "horizontal" => ["src" => "img1.jpg" , "data" => "xxxxxxxxxxxxxx"],
				    "squared" => ["src" => "img1.jpg" , "data" => "xxxxxxxxxxxxxx"]
			    ],
			    "authors" => ["1","2"],
			    "places" => [],
			    "times" => [],
			    "boards" => ["222","333","444","101010","181818"],
			    "keywords" => []
		    ],
				  
		    "board" => [
			    "id" => "111",
			    "name" => "authorGivenName",
			    "eventId" => "11",
			    "messages" => ["1111","1112","1113","1114","1115"]
		    ],
		  
		    "message" => [
			    "id" => "1111",
			    "boardId" => "111",
			    "authorId" => "1",
			    "reports" => ["aaaaa","bbbbb","ccccc","ddddd"]
		    ],
		  
		    "report" => [
			    "__comment" => "target field perchè i report vivono in uno spazio separato (navighi i report e da essi risali il flusso)",
			    "id" => "ABSOLUTE",
			    "from" => "whoReports",
			    "target" => "resourceURI"
		    ]
		];
		return json_encapsulate("schema",$schema);
	}
?>