<?php

class DatabaseHelper{
    public $db;

    public function __construct($servername, $username, $password, $dbname){
        $this->db = new mysqli($servername, $username, $password, $dbname);
        if($this->db->connect_error){
            die("Connesione fallita al db");
        }
    }
	
	/*è uno snippet preso dal corso di teconogie web
	
    public function checkLogin($username, $password){
        $stmt = $this->db->prepare("SELECT idautore, username, nome FROM autore WHERE username = ? AND password = ? AND attivo=1");
        $stmt->bind_param("ss", $username, $password);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }
	*/
}

//there is a fellowship graph between users(directional)
//there is a banList for each user, so a banGraph(let's make it nonDirectional)
//there is a tickets map :: users X tickets X events
//there is a bookmarks graph :: user X bookmarks = links

//se un evento sono utenti,
//allora gli eventi possono scrivere nelle board degli amici (amici degli admin)
//un evento può essere però visto da tutti

//to open an event,at least 3 persons must accept
//events does not follow people back
//events does not write to people
//events expire(kept in db but closed,the name is available again)

//there is a fellowship and ban graph for each board (board X graph X user)
//it is linked to users fellowships / bans :: ban between users => ban into respective boards

//================================================================
//get users (you are friend with)       || tu X fellowship
//get user by id                        || XXXXX
//get user by name                      || tu X fellowship X name
//befriend                              || fellowship += (tu,altro)
//unfriend                              || fellowship -= (tu,altro)
//get who you befriend                  || tu X fellowship
//get who befriended you                || fellowship X tu
//ban                                   || ban += (tu,altro)
//unban                                 || ban -= (tu,altro)
//bookmarks                             || tu X bookmarks
//set bookmark                          || bookmarks += (tu,link)
//remove from bookmark                  || bookmarks -= (tu,link)
// tickets = (ticketId,holderId)   ticket ha una scadenza, quindi nel mentre si sà qual'è l'evento associato
//your tickets                          || tu X tickets
//get ticket                            || tu X tickets X id
//use ticket (refund no/oppure 20%)     || tickets -= (id)



//get boards of an author               || get "id,name,description,thumbnailImg" from boards where authorId = xxx
//get a board by author                 || get "id,name,description,thumbnailImg" from boards where authorId = xxx and name = yyy
//create a new board                    || add "id,authorId,name,description,thumbnailImg" to boards
//update board info                     || put "name,description,thumbnailImg" to boards
//get all messages in a board           || get * from messages where boardId = XXX
//get all messages in a board by author || get * from messages where boardId = XXX and authorId = YYY
//create a new message in a board       || add "id,authorId,boardId,content,creationDate,lastModifyDate" to messages
//update a message                      || put content,lastModifyDate in messages where id = xxx 
?>