<?php
	require_once 'bootstrap.php';
	require_once 'db/utils.php';
	require_once 'db/outer/users.php';
	require_once 'db/outer/userRootQueryHandler.php';
	require_once 'db/outer/eventRootQueryHandler.php';

	function userRootFilter($string){
	  $usrRootRegex = '/^((?P<usrsSub>($|(?P<usrQuery>[a-zA-Z]+\/$)|((?P<usrId>\d+\/)(?P<usrSub>($|(?P<usrTickets>tickets\/($|(?P<ticketId>\d+\/)|(?P<ticketEvt>\w+\/)))$|(?P<usrInterests>interests\/$)|(?P<usrEvents>events\/))($|((?P<evtName>\w+\/)((?P<evtSub>$|(?<evtTickets>tickets\/$)|(?P<evtBoards>boards\/))(?P<brdsSub>$|(?P<brdName>\w+\/)(?P<brdSub>$|(?P<brdMessages>messages\/)))))))))))/';
	  preg_match($usrRootRegex, $string, $matches, PREG_UNMATCHED_AS_NULL);
	  
	  $keys = array("usrsSub","usrQuery","usrId","usrSub", "usrTickets",
	                "ticketId","ticketEvt","usrInterests","usrEvents",
					"evtId","evtName","evtSub","evtTickets","evtBoards",
					"brdsSub","brdName","brdSub","brdMessages");
	  $filtered = array();
	  
	  foreach($keys as $key){
		if(isSet($matches[$key])){
		  //ci tolgo lo slash finale
          $filtered[$key] = substr($matches[$key], 0, -1);
		}
	  }
	  return $filtered;
	}
	
	function interprete(){
	  $startString = $_SERVER['REQUEST_URI'];
	  ///'REQUEST_URI' = maglienowevents/api-NOW.php + /requset/path/		
	  //bisogna prima rimuovere il prefisso
	  $truncated = substr($startString, strlen("/maglienowevents/api-NOW.php"));
	  //resta ora (/bruttecose | /users/??? | /events/??? | /boards/??? | /messages/??? | /reports/??? )
	  $whichRootRegex = '/^\/((?P<legitRoot>(?P<userQuery>users\/)|(?P<eventQuery>events\/)|(?P<boardQuery>boards\/)|(?P<messageQuery>messages\/)|(?P<reportQuery>reports\/)|(?P<schema>schema\/))(?P<query>($|\S+)))/';
      $root = array();	 
	  preg_match($whichRootRegex, $truncated, $root, PREG_UNMATCHED_AS_NULL);
	    if(isSet($root["legitRoot"])){
			if(isSet(getallheaders()["Authorization"])){
		        $decoded = base64_decode(getallheaders()["Authorization"]); //base64_decode(str,STRICT=TRUE) will return FALSE if the input contains character from outside the base64 alphabet. Otherwise invalid characters will be silently discarded. 
   		        $req = explode( ":", $decoded);
			    $Users = new Users($GLOBALS["dbh"]->db);
   			    $requesterId = $Users->authenticate( $req[0], $req[1]);	

	            if(isSet($root["userQuery"])){
			        $solver = new UserRootQueryHandler();
			        $solver->solve($root["query"], $requesterId);
	            }else if(isSet($root["eventQuery"])){
			        $solver = new EventRootQueryHandler();
			        $solver->solve($root["query"], $requesterId);
	            }else if(isSet($root["boardQuery"])){
	                throw new Exception("PHP SAYS : boards/ as root not implemented yet");
	            }else if(isSet($root["messageQuery"])){
			        throw new Exception("PHP SAYS : messages/ as root not implemented yet");
	            }else if(isSet($root["reportQuery"])){
			        throw new Exception("PHP SAYS : reports/ as root not implemented yet");
	            }else if(isSet($root["schema"])){
			        echo json_schema();
	            }				
   	        }else{
   			    throw new Exception("NO AUTH");
   		    }
        }else{
	        header("Location: ../eventsNOW.php");
            die();
        }
	}
	
    try{
		interprete();
	} catch(Exception $e) {
		    echo json_failure($e->getMessage());
    }
?>