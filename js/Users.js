class User{
    fromData(id,username,email,type){
	    this.id = id;
	    this.username = username;
    	this.email = email;
	    this.type = type;
	}
	
	fromJSON(data){
		this.user(data["id"],data["username"],data["email"],data["type"]);
	}
	
	printAsLI(){
		var string = `<li id="users/${this.id}/">`
           	           + `${this.username} || ${this.email}`
				       + `<br/>`
				       + `id : ${this.id} || ${this.type}`
				   + `</li>`;
		return string;
	}
}