/*GET RANDOM INT INCLUSIVE FROM MOZILLA PAGES*/
function random(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min; //Il max è incluso e il min è incluso 
}

function randomColor(seed){
	switch(seed % 6){
		case 0: return "deepGreen";
		case 1: return "deepYellow";
		case 2: return "deepRed";
		case 3: return "deepMagenta";
		case 4: return "deepBlue";
		case 5: return "deepCyan";
	}
}

function listaEventi(data , start){
	let result="";
	for(let i=0; i < data.length; i++){
		let prodotto = `
		  <li class="card bg-transparent mb-1">
		    <article>
		        <button class="btn btn-link text-center text-light p-0" data-toggle="collapse" data-target="#collapse${i + start}" aria-expanded="true" aria-controls="collapse${i + start}">
              <div class="card-header p-0" id="heading${i + start}">
                  <div class="row no-gutters">
				    <div class="col-1 bg-danger py-2">
					  <p class="mx-0" style="transform: rotate(90deg);">07 set</p>
					</div>
					<div class="col-9">
					  <div class="container-fluid p-0">
					    <div class="row no-gutters">
						  <div class="col p-0">
						    <img class="img img-fluid" src="./upload/lizzy/jodorowski300_600.png" alt="so sorry,no alt"/>
						  </div>
						  <div class="col p-0">
						    <img class="img img-fluid" src="./upload/gallery/B1_col.png" alt="so sorry,no alt"/>
						  </div>
						  <div class="col p-0">
						    <div class="row d-block">
						    <img class="col img img-fluid float-left" src="./upload/lizzy/jodorowski300_${randomColor(random(0, 5))}.png" alt="so sorry,no alt"/>
							<img class="col img img-fluid float-left" src="./upload/lizzy/jodorowski300_${randomColor(random(0, 5))}.png" alt="so sorry,no alt"/>
							</div>
						  </div>
						  <div class="col p-0">
						    <img class="img img-fluid" src="./upload/lizzy/jodorowski300_600.png" alt="so sorry,no alt"/>
						  </div>
						  <div class="col p-0">
						    <img class="img img-fluid" src="./upload/gallery/A1_col.png" alt="so sorry,no alt"/>
						  </div>
						  <div class="col p-0">
						    <img class="img img-fluid" src="./upload/lizzy/jodorowski300_600.png" alt="so sorry,no alt"/>
						  </div>
						</div>
					  </div>
					</div>
					<div class="col-1 bg-dark">
					  <p class="mx-0" style="writing-mode: vertical-rl; text-orientation: upright;">5!</p>
					</div>
                                        <div class="col-1 bg-dark">
                                          <p class="mx-0" style="writing-mode: vertical-rl; text-orientation: upright;">+50</p>
					</div>
				  </div>
                </button>
              </div>
              <div id="collapse${i + start}" class="collapse bg-dark" aria-labelledby="heading${i + start}" data-parent="#products">
                <div class="card-body p-0">
				  <div id="carousel${i}" class="carousel slide text-center" data-ride="carousel">
                    <ol class="carousel-indicators">
                      <li data-target="#carousel${i}" data-slide-to="0" class="active"></li>
                      <li data-target="#carousel${i}" data-slide-to="1"></li>
                      <li data-target="#carousel${i}" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                      <div class="carousel-item bg-transparent p-0 active">
					    <button class="p-0 btn" data-toggle="modal" data-target="#eventModal">
						  <img class="img img-fluid" src="./upload/lizzy/jodorowski300_${randomColor(random(0, 5))}.png" alt="so sorry,no alt"/>
						</button>
                      </div>
                      <div class="carousel-item bg-transparent p-0">
					    <button class="p-0 btn" data-toggle="modal" data-target="#eventModal">
						  <img class="img img-fluid" src="./upload/lizzy/jodorowski300_${randomColor(random(0, 5))}.png" alt="so sorry,no alt"/>
						</button>
                      </div>
                      <div class="carousel-item bg-transparent p-0">
					    <button class="p-0 btn" data-toggle="modal" data-target="#eventModal">
						  <img class="img img-fluid" src="./upload/lizzy/jodorowski300_${randomColor(random(0, 5))}.png" alt="so sorry,no alt"/>
						</button>
                      </div>
                    </div>
                    <a class="carousel-control-prev" href="#carousel${i}" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel${i}" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>
                </div>
              </div>
			</article>
          </li>
		`;
		/*
		                  <p>${data[i]["art-body"]}</p>
				  <a class="btn btn-light text-dark">save</a>
				  <a class="btn btn-light text-dark">link</a>
				  <a class="btn btn-light text-dark">share</a>
		*/
		result += prodotto;
	}
	return result;
}

function events(from, to){
	$.getJSON("api-NOW.php?what=events&from=0&to=10", function(data){});
	data = [];
	stringa = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum";
	stringhe = stringa.split(" ");
	for( i = from; i < to; i++){
		dato = { "art-title" : stringhe[i] , "art-body" : stringa };
		data.push(dato);
	}
	return data;
}
