<!DOCTYPE html>
<html lang="it">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>EVENTONY</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<style>
		#my-nav-side {
			position: fixed; 
			bottom: 0px;
			right: 0px;
		}
		
		.effect8
        {
  	        position:relative;
            -webkit-box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
            -moz-box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
            box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
        }
        
		.effect8:before, .effect8:after
        {
	        content:"";
            position:absolute;
            z-index:-1;
            -webkit-box-shadow:0 0 20px rgba(0,0,0,0.8);
            -moz-box-shadow:0 0 20px rgba(0,0,0,0.8);
            box-shadow:0 0 20px rgba(0,0,0,0.8);
            top:10px;
            bottom:10px;
            left:0;
            right:0;
            -moz-border-radius:100px / 10px;
            border-radius:100px / 10px;
        }
	    .nav-item.active{background-color: #FFFFFF !important;}
		.nav-item:not(.active){background-color: #343a40 !important;}
			
		.bg {
			  background-image: url("./upload/cologne_underground.png");

			  background-position: center;
			  background-repeat: no-repeat;
              background-attachment: fixed;
			  background-size: cover;
			  
			}
			
		</style>
		<script src="js/jquery-3.4.1.min.js"></script>
	</head>
	<body class="bg m-0">
		<main class="container-fluid p-0 text-light">
			<div class="tab-content mt-3" id="nav-tabContent">
			  <section class="tab-pane fade" id="nav-user" role="tabpanel" aria-labelledby="nav-user-tab"><?php require "templates/user.php";?></section>
			  <section class="tab-pane fade show active" id="nav-events" role="tabpanel" aria-labelledby="nav-events-tab"  style="float: left;  max-width: 85vw; overflow-y: auto;"><?php require "templates/fastActions.php";?></section>
			  <section class="tab-pane fade" id="nav-support" role="tabpanel" aria-labelledby="nav-support-tab"><?php require "templates/support.php";?></section>
			</div>
		</main>
		<nav id="my-nav-side">
		  <div class="nav nav-tabs nav-justified flex-column" id="nav-tab" role="tablist">
			<button class="nav-item nav-link text-white p-1" id="nav-user-tab" data-toggle="tab" href="#nav-user" role="tab" aria-controls="nav-user" aria-selected="true" onclick="$('#nav-user-tab').removeClass('text-white').addClass('text-dark'); $('#nav-events-tab').addClass('text-white').removeClass('text-dark'); $('#nav-support-tab').addClass('text-white').removeClass('text-dark');"><img src="upload/icons/star-white.png"/></button>
			<button class="nav-item nav-link text-dark active p-1" id="nav-events-tab" data-toggle="tab" href="#nav-events" role="tab" aria-controls="nav-events" aria-selected="false"  onclick="$('#nav-user-tab').addClass('text-white').removeClass('text-dark'); $('#nav-events-tab').addClass('text-dark').removeClass('text-white'); $('#nav-support-tab').addClass('text-white').removeClass('text-dark');"><img src="upload/icons/rss-white.png"/></button>
			<button class="nav-item nav-link text-white p-1" id="nav-support-tab" data-toggle="tab" href="#nav-support" role="tab" aria-controls="nav-support" aria-selected="false"  onclick="$('#nav-user-tab').addClass('text-white').removeClass('text-dark'); $('#nav-events-tab').addClass('text-white').removeClass('text-dark'); $('#nav-support-tab').removeClass('text-white').addClass('text-dark');"><img src="upload/icons/gear6-white.png"/></button>
		  </div>
		</nav>
		<script type="text/javascript" src="templates/events.js"></script>
		<!--  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</body>
</html>