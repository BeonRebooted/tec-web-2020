<!DOCTYPE html>
<!--https://developer.mozilla.org/it/docs/Web/API/Element/textContent-->
<!--https://stackoverflow.com/questions/37044106/add-list-elements-dynamically-to-ul-element-->
<html>
<body>


<details>
    <summary>RIGHT ONES</summary>
    <ul id="id01"></ul>
</details>
<details>
    <summary>WRONG ONES</summary>
    <ul id="id02"></ul>
</details>
<details>
    <summary>BUG ONES</summary>
    <ul id="id03"></ul>
</details>

<script>
var rightOnes = ["users/",
                 "users/1/events/",
			     "users/1/events/albaTramonto/boards/",
			     "users/1/events/albaTramonto/boards/faq/messages/"];
			   
var wrongOnes = ["users/@%£COSESTRANE/",
                 "users/5/events/braciolata/@%£COSESTRANE/",
                 "users/5/events/braciolata/boards/@%£COSESTRANE/",
                 "users/5/events/braciolata/boards/main/@%£COSESTRANE/"];

var bugOnes = ["users/",
               "users/carlotta/", 
			   "users/3/",
			   "users/1/interests/",
			   "users/1/tickets/",
               "users/5/events/", 
			   "users/5/events/braciolata/",
			   "users/5/events/braciolata/tickets/",
               "users/5/events/braciolata/boards/",
			   "users/5/events/braciolata/boards/main/"
		       ];

var treatRightOnes = function() {
  if (this.readyState == 4 && this.status == 200) {
	  
	var results = JSON.parse(this.response);
	
	var list = document.getElementById('id01');
	var node = document.createElement("li");
	    var subHead = document.createElement("h3");
	        subHead.innerText =  this.responseURL;
	    node.appendChild(subHead);
		
	if(results.length > 0){
		var newList = document.createElement("ul");
		for(var i = 0; i < results.length; i++){
		  var item = document.createElement("li");
		  item.innerText = JSON.stringify(results[i]);
		  newList.appendChild(item);
	  }
	   node.appendChild(newList);
	}else{
	   var item = document.createElement("li");
	   item.innerText =  "FATAL ERROR";
	   node.appendChild(item);
	}
	list.appendChild(node);
  }
};

var treatWrongOnes = function() {
  if (this.readyState == 4 && this.status == 200) {
	  
	var results = JSON.parse(this.response);
	
	var list = document.getElementById('id02');
	var node = document.createElement("li");
	    var subHead = document.createElement("h3");
	        subHead.innerText =  this.responseURL;
	    node.appendChild(subHead);
		
	if(results.length > 0){
		var newList = document.createElement("ul");
		for(var i = 0; i < results.length; i++){
		  var item = document.createElement("li");
		  item.innerText = JSON.stringify(results[i]);
		  newList.appendChild(item);
	  }
	   node.appendChild(newList);
	}else{
	   var item = document.createElement("li");
	   item.innerText =  "FATAL ERROR";
	   node.appendChild(item);
	}
	list.appendChild(node);
  }
};

var treatBugOnes = function() {
  if (this.readyState == 4 && this.status == 200) {
	  
	var results = JSON.parse(this.response);
	
	var list = document.getElementById('id03');
	var node = document.createElement("li");
	    var subHead = document.createElement("h3");
	        subHead.innerText =  this.responseURL;
	    node.appendChild(subHead);
		
	if(results.length > 0){
		var newList = document.createElement("ul");
		for(var i = 0; i < results.length; i++){
		  var item = document.createElement("li");
		  item.innerText = JSON.stringify(results[i]);
		  newList.appendChild(item);
	  }
	   node.appendChild(newList);
	}else{
	   var item = document.createElement("li");
	   item.innerText =  "JS_says_no_match";
	   node.appendChild(item);
	}
	list.appendChild(node);
  }
};

var xmlhttp;
rightOnes.forEach(query=>{
	xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = treatRightOnes;
    xmlhttp.open("DELETE", "api-NOW.php/" + query, true);
	var encoded = btoa("beta@mocky.com:bbb");
	xmlhttp.setRequestHeader("Authorization", encoded);
    xmlhttp.send();
});

var xmlhttp;
wrongOnes.forEach(query=>{
	xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = treatWrongOnes;
    xmlhttp.open("DELETE", "api-NOW.php/" + query, true);
	var encoded = btoa("beta@mocky.com:bbb");
	xmlhttp.setRequestHeader("Authorization", encoded);
    xmlhttp.send();
});

var xmlhttp;
bugOnes.forEach(query=>{
	xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = treatBugOnes;
    xmlhttp.open("DELETE", "api-NOW.php/" + query, true);
	var encoded = btoa("beta@mocky.com:bbb");
	xmlhttp.setRequestHeader("Authorization", encoded);
    xmlhttp.send();
});
</script>




</body>
</html>