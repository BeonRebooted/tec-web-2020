<?php
require 'db/outer/users.php';
require 'db/outer/events.php';
require 'db/outer/messages.php';

$db = new mysqli("localhost", "root", "", "eventsnow");
if($db->connect_error){
    die("Connesione fallita al db");
}

$userManager = new Users($db);
echo $userManager->create("AUTHOR", "alpha@mocky.com", "anna", "aaa");
echo $userManager->create("USER", "beta@mocky.com", "bruno", "bbb");
echo $userManager->create("USER", "gamma@mocky.com", "carlotta", "ccc");
echo $userManager->create("AUTHOR", "delta@mocky.com", "damiano", "ddd");
echo $userManager->create("AUTHOR", "etha@mocky.com", "enza", "eee");


echo $userManager->getAll();
$users = json_decode($userManager->getAll());

echo $userManager->type(1);



$eventManager = new Events($db);
echo $eventManager->create("albaTramonto", "campagnaSoleCuore", "1970-01-01 00:00:01", "CASAMIA", "gallery/Unsplashdotcom/portrait/hardini-lestari-N3TypD-awJQ-unsplash.jpg", 1);
echo $eventManager->create("braciolata", "quella di anna", "1970-01-01 00:00:01", "CASAMIA", "jed-villejo-4SByp8kIoOE-unsplash.jpg", 1);
echo $eventManager->create("braciolata", "quella di enza", "1970-01-01 00:00:01", "alfonsine", "gallery/Unsplashdotcom/portrait/marc-babin-aQWmCH_b3MU-unsplash.jpg", 5);
echo $eventManager->create("cineforum alfonsine: La Casa", "la_casa", "1970-01-01 00:00:01", "alfonsine", "fidel-fernando-249DzAuJTqQ-unsplash.jpg", 5);
echo $eventManager->create("cineforum alfonsine: Madagascar", "madagascar", "1970-01-08 00:00:01", "alfonsine", "fidel-fernando-249DzAuJTqQ-unsplash.jpg", 5);
echo $eventManager->create("cineforum alfonsine: I Miserabili", "les miserables", "1970-01-15 00:00:01", "alfonsine", "fidel-fernando-249DzAuJTqQ-unsplash.jpg", 5);
echo $eventManager->create("cineforum alfonsine: Top Gun", "con tom cruise", "1970-01-22 00:00:01", "alfonsine", "fidel-fernando-249DzAuJTqQ-unsplash.jpg", 5);
echo $eventManager->create("cineforum alfonsine: Hot Shot", "Wambo", "1970-01-29 00:00:01", "alfonsine", "fidel-fernando-249DzAuJTqQ-unsplash.jpg", 5);
echo $eventManager->create("cineforum alfonsine: Rambo", "quello vero", "1970-02-05 00:00:01", "alfonsine", "fidel-fernando-249DzAuJTqQ-unsplash.jpg", 5);
echo $eventManager->create("cineforum alfonsine: Sherlock Holmes", "primo", "1970-02-12 00:00:01", "alfonsine", "fidel-fernando-249DzAuJTqQ-unsplash.jpg", 5);
echo $eventManager->create("cineforum alfonsine: Sherlock Holmes DUE", "secondo", "1970-02-19 00:00:01", "alfonsine", "fidel-fernando-249DzAuJTqQ-unsplash.jpg", 5);
echo $eventManager->create("cineforum alfonsine: Sherlock Holmes TRE", "terzo", "1970-02-26 00:00:01", "alfonsine", "fidel-fernando-249DzAuJTqQ-unsplash.jpg", 5);
echo $eventManager->create("cineforum alfonsine: Sherlock Holmes TREBBIANO", "boh", "1970-03-05 00:00:01", "alfonsine", "fidel-fernando-249DzAuJTqQ-unsplash.jpg", 5);
echo $eventManager->create("cineforum alfonsine: Teatro", "ONE NIGHT ONLY", "1970-03-12 00:00:01", "alfonsine", "fidel-fernando-249DzAuJTqQ-unsplash.jpg", 5);

echo $eventManager->getAll(); //voglio coppie id,nome (gli autori sono extra)




$messageManager = new Messages($db);
//ogni evento genera 3 board nel seguente ordine : main,faq,after
//create(boardId,content,authorId)
$messageManager->create(1, "un giorno in campagna", 1);
$messageManager->create(1, "ciaoooo, ma quanto tempo!!!", 3);
$messageManager->create(1, "non vedo l'ora", 3);
$messageManager->create(2, "ci sono indicazioni lungo la strada? ", 2);
$messageManager->create(2, "si,ci saranno degli addobbi a segnare la discesa.", 1);
$messageManager->create(7, "fai a gara di eventi???", 1);

?>