<!DOCTYPE html>
<!--https://developer.mozilla.org/it/docs/Web/API/Element/textContent-->
<!--https://stackoverflow.com/questions/37044106/add-list-elements-dynamically-to-ul-element-->
<html>
<body>

<H2>RIGHT ONES</H2>
    <ul id="id01"></ul>

<script>
var rightOnes = ["users/",
                 "users/anna/",
                 "users/2/",
				 "users/1/friendList/",
				 "users/1/friendlist/",
				 "users/1/interests/",
			     "users/1/bookmarks/",
			     "users/1/events/",
				 "users/1/tickets/",
			     "users/1/events/albaTramonto/",
			     "users/1/events/albaTramonto/boards/",
			     "users/1/events/albaTramonto/boards/faq/messages/",
			     "users/5/events/braciolata/boards/",
			     "users/5/events/braciolata/boards/main/",
			     "users/5/events/braciolata/boards/main/messages/",
				 "users/1/events/braciolata/tickets/",
				 "users/5/events/albaTramonto/boards/faq/",
				 "events/",
			     "events/11/",
			     "events/11/boards/",
			     "events/11/boards/main/",
			     "events/11/boards/main/messages/",
			     "boards/",
			     "boards/31/",
			     "boards/31/messages/",
			     "messages/",
			     "messages/1/",
			     "reports/"
				 ];

var treatRightOnes = function() {
  if (this.readyState == 4 && this.status == 200) {
	  
	var results = JSON.parse(this.response);
	
	var list = document.getElementById(this.responseURL);
		
	if(results.length > 0){
		var newList = document.createElement("ul");
		for(var i = 0; i < results.length; i++){
		  var item = document.createElement("li");
		  item.innerText = JSON.stringify(results[i]);
		  newList.appendChild(item);
	  }
	   list.appendChild(newList);
	}else{
	   var item = document.createElement("li");
	   item.innerText =  "FATAL ERROR";
	   list.appendChild(item);
	}
  }
};


var xmlhttp;
var list = document.getElementById('id01');
rightOnes.forEach(query=>{
	var fullString = "http://localhost/maglienowevents/api-NOW.php/" + query;
	var node = document.createElement("li");
	var dtl = document.createElement("details");
	dtl.id = fullString;
	var sum = document.createElement("summary");
	sum.innerText = query;
	dtl.appendChild(sum);
	node.appendChild(dtl);
	list.appendChild(node);
	xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = treatRightOnes;
    xmlhttp.open("POST", fullString, true);
	var encoded = btoa("beta@mocky.com:bbb");
	xmlhttp.setRequestHeader("Authorization", encoded);
    xmlhttp.send();
});

</script>
</body>
</html>